import os
import sys
import yaml

def parse_yaml():
    with open('texv.yaml') as yst:
        try:
            print(yaml.safe_load(yst))
        except yaml.YAMLError as er:
            print(er)


parse_yaml()
